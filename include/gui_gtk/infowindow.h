/*****************************************************************
 * gmerlin - a general purpose multimedia framework and applications
 *
 * Copyright (c) 2001 - 2012 Members of the Gmerlin project
 * gmerlin-general@lists.sourceforge.net
 * http://gmerlin.sourceforge.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * *****************************************************************/


#ifndef __BG_GTK_INFOWINDOW_H_
#define __BG_GTK_INFOWINDOW_H_

typedef struct bg_gtk_info_window_s bg_gtk_info_window_t;

/* State */
#define BG_GTK_INFOWINDOW_STATE_CTX "infowindow"
#define BG_GTK_INFOWINDOW_VISIBLE   "visible"

bg_gtk_info_window_t *
bg_gtk_info_window_create(bg_player_t * player);

void bg_gtk_info_window_destroy(bg_gtk_info_window_t *);

/* Show/hide the window */

void bg_gtk_info_window_show(bg_gtk_info_window_t *);
void bg_gtk_info_window_hide(bg_gtk_info_window_t *);

void bg_gtk_info_window_init_state(gavl_dictionary_t * dict);

void
bg_gtk_info_window_disconnect(bg_gtk_info_window_t * w);

void
bg_gtk_info_window_connect(bg_gtk_info_window_t * w, bg_controllable_t * ctrl_p);


#endif // __BG_GTK_INFOWINDOW_H_
